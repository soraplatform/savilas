---
title: "Octopus"
# eef Steak with Mushroom
# date: 2022-04-17T11:22:16+06:00
description : "Octopus is a popular export product of Vietnam"
type: "menu"
# cost: "Php 150"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
images:
  - image: "t/fish3.jpg"
---
