---
title: "FAQ"
# description : "FAQ for Elivir's Lechon"
video:
  videoThumb : "/t/fish1.jpg"
  # videoURL : "https://www.youtube.com/embed/rMNAF1AzzZY"
---


## What is the inspiration for Savilas?

Savilas was created by Linh Nguyen to help foreigners who would like to import Vietnamese seafood and other food items.  

The increase in food prices and rising hunger in the world has increased the demand for nutritious but inexpensive Vietnamese food in the world market. We aim to help address such urgent needs. In this way, we can help the international market, as well as support Vietnamese fishermen and food processors.  

<!-- It focuses on exporting seafood from Vietnam to address hunger
 -->

<!-- ## Do you have any advocacies?

Yes! We advocate:
- Food Safety and Nutrition
- Environmentalism
- Increasing employment -->

## What are your main product offerings?

We have 3 categories:

1. Raw seafood such as Shrimp, Octopus/Cuttlefish, Shellfish, Tuna, Small salwater fish such as yellow tail scad, Indian mackerel, red mullet, pomfret, etc.

2. Value-added seafood such as Skewed cuttlefish/octopus, Tempura Shrimp, Coconut Shrimp, Shrimp Rings, etc. 

3. Canned seafood such as shredded tuna can with sunflower oil


## Where do you source your supplies?

We get our supplies from reputatable seafood factories in Vietnam, accredited by the Vietnam Seafood Exporters and Producers Association. These have  factory certifications issued by ASC, Nafiquad, etc. <!--  with safe resource, trade ability and high trust in the market. -->


## What services do you offer?

We act as your reliable contact who can help you check suppliers in Vietnam.

We help you decide when to import or purchase by providing relevant information about the source of the seafood.

We help you growth your revenue by researching potential products that can expand your ability to compete with your competitors.

We help reduce costs by finding alternative suppliers that can bring same value with a more competitive price.

<!-- • Be your trust assistant in Vietnam, high responsibility, more than your expectation. -->


## Our Market

We cater to the following people and organizations: 

1. Big companies who want to expand seafood business using imported Vietnam seafood
2. Seafood start-ups who will import Vietnamese seafood for the first time and don’t have much knowledge about Vietnam
3. Seafood companies would like to avoid scams and bad producers, bad quality products from Vietnam 
4. Seafood buyers who want to expand their product list of suppliers in Vietnam
5. Seafood buyers who want to change your seafood supplier and find reliable producer who can have a long-term relationship based on trust and mututal support


<!--     • Cost effective
    • Good relationship with big range of seafood producer who ready & stable resource for your business.
    • Trust
    • Do everything with best possible to save benefit of customer, even many times we have to scarify our benefit. -->

## Social Responsibility

We commit 10% of our profit every month for 3 kinds of social donations:

1. Education
2. Climate change
3. Charity 

We document all our Social Responsibility activities. 

<!-- We will make a new bank account only spend for donate and update status to all our partner. -->



Image of founder: me and my friend. Will be update later