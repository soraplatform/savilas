---
title: "Skewed Cuttlefish and Octopus"
# date: 2022-04-17T11:22:16+06:00
description : "Skewed Cuttlefish and Octopus has a high value both locally and overseas"
type: "menu"
# cost: "Php 125"
# costdef: "1 point = 1 value of 1 kilo NFA rice via [the Pantrypoints system](https://pantrypoints.com)"
cta: "Contact us to Inquire"
# first image will be shown in the product page
images:
  # - image: "noimg.png"
  - image: "t/fish4.jpg"
---

